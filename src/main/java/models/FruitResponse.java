package models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FruitResponse {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;
}