package helper;

import models.FruitResponse;

public class FruitsDataHelper {
    public static FruitResponse getExpectedAppleResponse() {
        return FruitResponse.builder()
                .provider("Jumbo")
                .title("Kanzi Appels en Mandarijnen")
                .url("https://jumbo.com/Kanzi-Appels-en-Mandarijnen/441724STK")
                .brand(null)
                .price(5.48)
                .unit("1 st.")
                .isPromo(false)
                .promoDetails("").image("https://ish-images-static.prod.cloud.jumbo.com/product_images/030520211552_441724STK-1_360x360.png")
                .build();
    }
}
