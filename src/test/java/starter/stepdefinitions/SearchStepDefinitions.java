package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.FruitResponse;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import static helper.FruitsDataHelper.getExpectedAppleResponse;
import static java.util.Arrays.*;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;

public class SearchStepDefinitions {

    @Steps
    public FruitsAPI fruitsAPI;
    List<FruitResponse> fruits;

    @When("I call endpoint {string}")
    public void iCallEndpoint(String arg0) {
        fruits = asList(fruitsAPI.get(arg0).getBody().as(FruitResponse[].class));
    }

    @Then("I see the results displayed for apple")
    public void iSeeTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
        assertTrue(fruits.contains(getExpectedAppleResponse()));
    }

    @Then("I see no results")
    public void iSeeNoResults() {
        restAssuredThat(response -> response.body("error", contains("True")));
        assertTrue(fruits.isEmpty());
    }
}
