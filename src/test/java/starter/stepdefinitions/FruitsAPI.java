package starter.stepdefinitions;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class FruitsAPI {

    public static final String host = "https://waarkoop-server.herokuapp.com/api/v1/search/test/";

    @Step
    public Response get(String endpoint) {
        return SerenityRest.given().get(host + endpoint);
    }
}
