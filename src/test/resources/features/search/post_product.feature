Feature: Search for the product

  ###Positive
  Scenario:
    When I call endpoint "apple"
    Then I see the results displayed for apple
  ###Negative
  Scenario:
    When I call endpoint "anything"
    Then I see no results
