#Recent changes

1. Fixed dependencies in pom.xml
2. Simplified features calls - only endpoint is needed to pass
3. We can have predefined test data in helper, deserialize response to model and make data assertions
4. We can replace data helper with mocks or with JUnit data providers if needed
5. Enriched FruitsAPI class. If we need to enrich SerenityRest configuration (e.g. auth) we can do it there
6. Removed excessive strings from pom.xml
7. Added Gitlab CI configuration:
   1. Compile
   2. Run tests 
   3. Get Serenity report 
   4. Aggregate Serenity report as HTML page
   5. Publish
8. How to run: 
   - On GitlabCI - in pipeline with gitlab-ci.yml
   - Locally - use command "mvn clean test site" and "mvn serenity:aggregate"